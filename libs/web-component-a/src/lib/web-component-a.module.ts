import { ApplicationRef, DoBootstrap, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [CommonModule],
})
export class WebComponentAModule implements DoBootstrap {
  
  ngDoBootstrap(appRef: ApplicationRef): void {
    
  }

  
}
