import { ApplicationRef, DoBootstrap, Injector, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Component1Component } from './component1/component1.component';
import { createCustomElement } from '@angular/elements';
import { BrowserModule } from '@angular/platform-browser';


@NgModule({
  imports: [CommonModule, BrowserModule],
  declarations: [Component1Component],
})
export class WebComponentBModule implements DoBootstrap {
  constructor(injector: Injector) {
    const custom = createCustomElement(Component1Component, { injector });
    customElements.define('component-1', custom);
  }
  ngDoBootstrap(appRef: ApplicationRef): void {}
}
