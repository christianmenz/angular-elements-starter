import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'elements-component1',
  templateUrl: './component1.component.html',
  styleUrls: ['./component1.component.css'],
  encapsulation: ViewEncapsulation.ShadowDom

})
export class Component1Component implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
