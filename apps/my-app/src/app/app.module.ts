import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { WebComponentBModule } from '@elements/lib1';
import  {WebComponentAModule} from '@elements/web-component-a';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, WebComponentAModule, WebComponentBModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
